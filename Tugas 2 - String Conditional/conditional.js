// Tugas conditional

// if else

var nama = "John"
var peran = "Penyihir"

if (nama.length == 0 && peran.length == 0) {
	console.log("nama harus diisi!")
} else if (nama.length > 0) {
	if (peran.length == 0) {
		console.log("Halo "+nama+", Pilih peranmu untuk memulai game!")
	} else {
		if (peran == "Penyihir") {
			console.log("Selamat datang di Dunia Werewolf, "+nama)
			console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa yang mejadi werewolf!")
		} else if (peran == "Guard") {
			console.log("Selamat datang di Dunia Werewolf, "+nama)
			console.log("Halo "+peran+" "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.")
		} else if (peran == "Werewolf") {
			console.log("Selamat datang di Dunia Werewolf, "+nama)
			console.log("Halo "+peran+" "+nama+", Kamu akan memakan mangsa setiap malam!")
		}
	}
}



// switch case

var tanggal = 12
var bulan = 5
var tahun = 2010
switch (bulan) {
	case 1: { console.log(tanggal.toString()+" Januari "+tahun.toString()); break; }
	case 2: { console.log(tanggal.toString()+" Februari "+tahun.toString()); break; }
	case 3: { console.log(tanggal.toString()+" Maret "+tahun.toString()); break; }
	case 4: { console.log(tanggal.toString()+" April "+tahun.toString()); break; }
	case 5: { console.log(tanggal.toString()+" Mei "+tahun.toString()); break; }
	case 6: { console.log(tanggal.toString()+" Juni "+tahun.toString()); break; }
	case 7: { console.log(tanggal.toString()+" Juli "+tahun.toString()); break; }
	case 8: { console.log(tanggal.toString()+" Agustus "+tahun.toString()); break; }
	case 9: { console.log(tanggal.toString()+" September "+tahun.toString()); break; }
	case 10: { console.log(tanggal.toString()+" Oktober "+tahun.toString()); break; }
	case 11: { console.log(tanggal.toString()+" November "+tahun.toString()); break; }
	case 12: { console.log(tanggal.toString()+" Desember "+tahun.toString()); break; }
	default: {console.log("Tidak ada bulan "+bulan);}
}
