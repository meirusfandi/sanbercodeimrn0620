// Task Day 5 - Array

// Question and Answer No. 1
function range(num1, num2) {
	var angka = []
	if (typeof(num1) === 'undefined' && typeof(num2) === 'undefined') {
		return -1
	} else if (typeof(num1) === 'undefined' || typeof(num2) === 'undefined') { 
		return -1
	} else if (num1 > num2) {
		for (var i=num1; i>=num2; i--) {
			angka.push(i)
		}
	} else if (num1 < num2) {
		for (var i=num1; i<=num2; i++) {
			angka.push(i)
		}
	}
	return angka;
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())



// Question and answer no 2
function rangeWithStep(num1, num2, step) {
        var angka = []
        if (num1 > num2) {
                for (var i=num1; i>=num2; i-=step) {
                        angka.push(i)
                }
        } else if (num1 < num2) {
                for (var i=num1; i<=num2; i+=step) {
                        angka.push(i)
                }
        }
        return angka;
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))


// Question and answer no 3
function sum(num1, num2, step) {
        var jumlah = 0
        if (typeof(num1) === 'undefined' && typeof(num2) === 'undefined' && typeof(step) === 'undefined') {
                return 0
        } else if (typeof(step) === 'undefined' && typeof(num2) === 'undefined') {
                return num1
        } if (typeof(step) === 'undefined') {
		if (num1 > num2) {
                	for (var i=num1; i>=num2; i--) {
                        	jumlah += i
                	}
        	} else if (num1 < num2) {
                	for (var i=num1; i<=num2; i++) {
                        	jumlah += i
                	}
        	}
	} else if (num1 > num2) {
                for (var i=num1; i>=num2; i-=step) {
                        jumlah += i
                }
        } else if (num1 < num2) {
                for (var i=num1; i<=num2; i+=step) {
                        jumlah += i
                }
        }
        return jumlah;
}

console.log("sum 1 " + sum(1,10)) // 55
console.log("sum 2 " + sum(5, 50, 2)) // 621
console.log("sum 3 " + sum(15,10)) // 75
console.log("sum 4 " + sum(20, 10, 2)) // 90
console.log("sum 5 " + sum(1)) // 1
console.log("sum 6 " + sum()) // 0 


// Question and answer no 4
//contoh input
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 

for (var i=0; i<input.lengh; i++) {
	for (var j=0; j<4; j++) {
		if (j == 0) {
			console.log("Nomor ID : "+input[i][j])
		} else if (j == 1) {
			console.log("Nama Lengkap : "+input[i][j])
		} else if (j == 2) {
			console.log("TTL : "+input[i][j]+" "+input[i][j+1])
		} else if (j == 3) {
			console.log("Hobi : "+input[i][j+1])
		}
	}
	console.log("")
}


// Question and answer no 5
function balikKata(kata) {
	var katabaru = ''
	for (var i=kata.length - 1; i>=0; i--) {
		if (i == kata.length) { katabaru = kata[i] } else { katabaru = katabaru + kata[i]}
	} 

	return katabaru
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
