// Tugas 3 - Looping

// Soal 1
// Jawaban
var nilai = 0

console.log("LOOPING PERTAMA")
while(nilai < 20) {
	nilai+=2
	console.log(nilai.toString()+" - I love coding")
}

console.log("LOOPING KEDUA")
while(nilai > 0) {
	console.log(nilai.toString()+" - I will become a mobile developer")
	nilai-=2
}

// Soal dan Jawaban No 2

for (var i=1; i<=20; i++) {
	if (i % 2 == 0) {
		console.log(i.toString()+" - Berkualitas")
	} else {
		if (i % 3 == 0) {
			console.log(i.toString()+" - I Love Coding")
		} else {
			console.log(i.toString()+" - Santai")
		}
	}
}


// Soal dan Jawaban No 3
for (var i=0; i<4; i++) {
	var hasil = ""
	for (var j=0; j<8; j++) {
		hasil = hasil + "#"
	}
	console.log(hasil)
}


// Soal dan Jawaban No 4
for (var i=0; i<7; i++) {
	var hasil = ""
	for (var j=0; j<=i; j++) {
		hasil = hasil + "#"
	}
	console.log(hasil)
}


// Soal dan Jawaban No 5
for (var i=0; i<8; i++) {
	var hasil = ""
	for (var j=0; j<8; j++) {
		if (i % 2 == 0) {
			if (j % 2 == 0) { hasil = hasil + " " } else { hasil = hasil + "#" }
		} else {
			if (j % 2 == 1) { hasil = hasil + " " } else { hasil = hasil + "#" }
		}	
	}
	console.log(hasil)
}
