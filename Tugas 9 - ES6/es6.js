// Task 9 - ES6

// Petunjuk : 
// Ubahlah Code berikut dari ES5 ke ES6:

// Question and Answer No 1
const golden = () => {
    console.log("this is golden!!")
}
golden()

// Question and answer No 2
// return dalam fungsi di bawah ini masih menggunakan object literal dalam ES5, 
// ubahlah menjadi bentuk yang lebih sederhana di ES6.

const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName : () => {
            console.log(firstName + " " + lastName)
            return 
        }
    }
}
//Driver Code 
newFunction("William", "Imoh").fullName() 


// Question and answer No 3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation} = newObject

// Driver code
console.log(firstName, lastName, destination, occupation)

// Question and answer No 4
// Kombinasikan dua array berikut menggunakan array spreading ES6
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
const combined = [...west, ...east]
//Driver Code
console.log(combined)


// Question and answer No 5
// sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:
const planet = 'earth'
const view = 'glass'
var before = `Lorem ${view} dolor sit amet, ` +  
    `consectetur adipiscing elit, ${planet} do eiusmod tempor ` +
    `incididunt ut labore et dolore magna aliqua. Ut enim` +
    ` ad minim veniam`

// // Driver Code
console.log(before) 
